# Requirements Report
 
 Module Code: CS1SE17 

 Assignment report Title: Requirements Report
 
 Student Numbers (e.g. 25098635): 28007707, 28019011, 27820418 , 26001722
 
 Date (when the work completed): 31/10/19
 
 Actual hrs spent for the assignment: 40
 
 Assignment evaluation (3 key points): 
 
## Abstract
A Report detailing issues and queries that will need to be reaised with the 
client in order to progress further into th project. This has been split into 
three questions: 
1. Who should you be asking about the requirements?
2. What points do you need to clarify?
3. What constraints might there be?
4. Which points need clarification first?

## Background
Every day, thousands of university students are given set readings to do in 
order to help aid their learning. There is no easy set way of knowing whether 
a student has done the reading or not, without testing them, which takes 
valuable learning time to do so. After all, it’s their education so ultimately
is down to them. Therefore, it is essential that students complete the required 
readings in order to get the best possible start in their lectures to come.


## Introduction
The job given to us as Software Engineers is to create an educational video game 
that will encourage university students to do their set readings. 
However, the customer was very vague with his demand, leaving the planning of 
the project difficult to do. Project Planning is the most important aspect in 
Software engineer projects so before we can start working on the project, we 
need to ask some questions in order to clarify the job we have to do and be 
able to plan our project efficiently.


## Question 1
In the video gaming industry, having feedback from members of the public is very
important, as it gives the project a sense of direction and a clear 
understanding on what the customer is looking for. Video games are a ubiquitous 
part of almost all children’s and adolescents’ lives.  
(Granic, Lobel, & Engels, 2014).

Market research is about gathering, analysing and disseminating information on 
users and potential users and feedback on their wants, needs and perceptions. 
It consists of both academic treatises and practical approaches to the 
collection, analysis, interpretation, and use of data. Contrary to conventional 
beliefs that playing video games is intellectually lazy and sedating, it turns 
out that playing these games promotes a wide range of cognitive skills. 
(Steenburgh & Wittink, 2001).  By developing a game that will benefit the 
student, it will enable them to widen their knowledge and tackle problems. 

Since the customer is interesting in having an educational video game that will 
encourage University Students, the best members of the public to discuss 
suggestions for a game are University Students. Video games aren’t just for 
kids. For students, games are as much a part of life as studying and partying. 
A study from Pew Internet Research finds that 70 percent of college students 
play video games at least “once in a while.”(Weaver, 2013). What’s helpful is 
that there are so many genres, types and modes in games nowadays that it will 
broaden the search for the perfect game combination. The puzzle genre can be 
massively built upon with a lot of features, rewards and influence the player’s 
mind and thinking. 

Not only will asking students help with Market research in developing the game, 
the best approach is to ask both Non gamers and gamers. This way we’ll have two 
different visions of how students’ what features, gameplay elements and the 
general aesthetic of a puzzle-based game.

Another area of research which will benefit the development are game developers.
Specifically, developers who have worked on puzzle-based games since they know 
the mechanics and understanding on what goes on in a game based around puzzles. 
Not only will asking a developer with experience in that field will benefit the 
creation, it also allows us to ask questions on how the development of a video 
game takes, the processes of tackling each individual task and how long each 
task will take to complete. 

The final class of market research is the customer themselves. It is always best
to ask the customer what their view is on their own product. Assuming that they 
have attended University, asking the customer the same questions asked to the 
students about what they’d like to see in an interactive way of doing set 
readings.

## Question 2
Many different points need to be clarified in order to create a software that 
will satisfy the customer’s needs, as well as to help the planning of the 
project.

We need to know the complexity of the game, whether it will be a simple browser 
game or a game with 3D graphics. Knowing how long the game is supposed to be is
also valuable information. The effort required for the project will differ 
based on the complexity and length of the game and as such we need to know how 
complex the game will be to properly prepare for the project .

The subject of the game also needs to be clarified. In this case we know it is 
about the history of the Atlantic but what parts of its history? Do we tackle 
the entirety of the history of the Atlantic or only certain parts of it? Pirates
and slave trade were mentioned as examples but are we only supposed to include 
those parts of the history of the Atlantic? The history of the Atlantic is a 
rather vast and vague subject which increases the chances of making a software 
which does not satisfy the customer which as software engineers we want to 
avoid.

The difficulty also needs to be clarified: how hard are we supposed to make the 
game? We also need to know whether the game will be under the form of a 
challenge that will test their knowledge of the game, or whether their 
isn’t tested but numerous facts are implemented in the game to support them in 
learning about the subject. These are two different manners of learning and how 
we're supposed to make the educational part of the game has not been clarified.   

Knowing a bit more about the students that will be playing our game will also 
help us in the developement of the project. Are these students first years, 
second years or third years? Are they mostly international students? We could 
tweak the difficulty and maybe even add different languages based on the 
feedback we get from these questions. 

We also haven’t been told the amount of time we have to do this project. Knowing
how much time we have is very important when it comes to planning. In fact, we 
can’t do any planning without knowing how much time we have to do the project. 

Also do we want to include any form of audio in the game? If so, do we want an 
original soundtrack for it? We need to know whether we have to hire someone 
adept to making the soundtrack for this game, which will increase the cost of 
the project.

All of these clarifications are important as without them we cannot be sure to 
deliver a software that will satisfy the customer's needs. We also need these 
clarifications to be able to calculate the costs and plan efficiently.

"Before starting a software project, it is essential to determine the tasks to 
be performed and properly manage allocation of tasks among individuals involved 
in the software development. Hence, planning is important as it results in 
effective software development." 
([Dinesh Thakur 2015](http://ecomputernotes.com/software-engineering/project-planning))

## Question 3
Within the development of any given project there are always a set of 
constraints that may potentially limit the scope, effectiveness, and overall 
success of the project. Due to the nature of this project there are a number of 
possible constraints that should be addressed. These include legal constraints, 
technical constraints, time and budgeting constraints, as well as overall 
quality constraints.

#### Legal Constraints
This software will be using the set readings as a resource from the project.
This means that if the projects contains any of this data we have to check 
if we are licenced to use it in a commercial setting. For example the Book 
Catherine Amstrong & Laura M Chmielewski, 2013, “The Atlantic Experience: 
peoples, places, ideas” has a copyright which is subject to the Copyright, 
Designs, and Patents act of 1988. If we were to use the information or 
excerpts of the book in the game we would have to contact the Author and 
obtain written permission in order to use it 
([Copyright, Design, and Patents act 1988](http://www.legislation.gov.uk/ukpga/1988/48/pdfs/ukpga_19880048_en.pdf)). 
Because of the nature of 
Copyright every book or resource that we want to use will have to be checked
and permission obtained.

#### Technical Constraints
As of yet we do not know what the “video” game will entail. Weather the 
client wants a 2D game with little scope or a 3D game with huge amount of 
playtime. The decision of what the gameplay will actually be will be the 
major factor in deciding what technical constraints will be imposed on the 
project. If the scope of the game is too big then the skill of the 
programmers might not be enough to handle it. There might also be a point 
where if the game is overdeveloped then it may not be able to be run on all
machines without crashing. 

#### Time and Budgeting Constraints
Depending on the aforementioned scope of the project there might not be 
enough time to successfully deliver a finished project for the budget that 
has been planned. A good model for this might be time ∝ budget. As the 
amount of time spent goes up the amount of budget increases. We will have to
discuss the required time limit with the client in order to set a budget. 

#### Quality Constraints
As the scope of the project expands then the overall quality will decrease 
with a set time. Once we know what the project will be and the timeframe 
that we have we can begin to estimate the quality of the finished product. 
If the time is too short or the scope is too big then there will not be a 
high quality project at the end of development. We should decide with the 
client and adequate time frame and scope so that we can deliver a good 
product.

## Question 4
Having a meeting with the client to talk about what points need clarification 
first is a fundamental part of creating a successful project. The groundwork 
needs to be set a laid out in order to make sure that each subsequent addition 
to the project has the correct resources needed and already implemented, 
meaning when it comes time to develop, you don’t have to halt a stage of a 
project to work on another aspect which should’ve retrospectively been 
completed first. A clientele meeting should be there to catch these oversights
before you start the project and run into them. 

First and foremost, the most important point that needs clarification first 
is the price and timescale of the project. How much is the client willing to 
pay for the project, and what time frame should it be completed in. They may 
be interested in throwing in a lot of money so that it can be completed in as 
little time as possible, at which point you may have to discuss the issues 
with this approach and how it may lead to bug testing issues later down the 
line. Or whether they’d prefer the project to take its time so that it’s as 
truly polished off and finished as it can be. You may want to mention if it
requires continuous updating and maintenance. 

The type of game is very important as it ultimately will decide the user 
experience. It is of course based largely off the price of the project, and 
time constraints. So, the final project will vary widely depending on this. 
A 3D online multiplayer game is obviously going to take longer and require 
much more money to develop compared to just a single player 2D flash game. 
This is where they type of game desired should be talked about and the
mechanics behind how it should work. Should it be point and click, quiz based, 
movable character with the WASD keys, etc. If an online option is chosen, with 
high scores saved making it competitive. You’d have to ensure that the game is
General Data Protection Regulation (GDPR) compliant 
([European General Data Protection Regulation of 2016](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32016R0679&from=EN#d1e3434-1-1)) 
Ensuring data only 
necessary is saved, and if there was to be any data breach all students will 
be notified immediately. 

The size, length and complexity of the game should be established very early on,
as it sets the scope for how the game will ultimately turn out. Should it just 
be a simple minigame to test whether the students have done their reading, and
it just tests their knowledge. Or should it be used as a primary resource for 
the students to do the readings first-hand. The main educational goals of the 
game should also be discussed at this point. What readings need to be 
implemented, and how important aspects of the reading are conveyed across to 
the students. 

Versatility of the game requires very important clarification. Does each
reading need to be hard coded and implemented into the game, or should it be 
customisable and have the ability to change the readings based on an input 
file that the lecturer can specify? This would add great complexity to the 
development but mean that the game could be used in many more cases, and not 
only be just reading specific, but subject specific either. 

From the brief we already know that the target audience is University Students,
but the level of the student has not been specified. This is when you’d discuss
the which level of university student would be using the program. Obviously, 
there would be no need to include patronising aspects that a pre-school
“learn-to-read" game would have, but a Postgraduate or even PHD student would
have different levels of abilities and understanding than Undergraduates. 
This also links back to the idea of versatility and how it could be desirable
that it’s adaptable. 

Clarification of difficulty of the game then comes next. After finding out 
the specific target audience, we can then begin to understand how difficult 
the game should be. Not only from a game interaction point of view, but also 
the form the content it teaches. Should it be adjustable difficulty, if so,
should the user be allowed to select their own difficulty, or should the
difficulty be set by the lecturer?  Or should it have dynamic difficulty,
meaning that if the student gets lots of questions wrong, it will adapt and
change the questions making them easier. Vice versa if the student is finding 
the questions really easy and getting them all correct, should it make 
them harder? 

It is very important that the right things get clarified first as that means 
as soon as they are, work can get started on the problem immediately, and
there is no waste of time developing aspects of a project that subsequently
need to be removed. After much discussion this order is optimal as it 
focuses on key technical features about the project, and then ultimately 
finishes with ideas which are nice for the client to consider and could 
still produce a very functional game without the in-depth clarification 
of the point in question. 


 
 